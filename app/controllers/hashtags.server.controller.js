'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Hashtag = mongoose.model('Hashtag'),
	_ = require('lodash');

/**
 * Get the error message from error object
 */
var getErrorMessage = function(err) {
	var message = '';

	if (err.code) {
		switch (err.code) {
			case 11000:
			case 11001:
				message = 'Hashtag already exists';
				break;
			default:
				message = 'Something went wrong';
		}
	} else {
		for (var errName in err.errors) {
			if (err.errors[errName].message) message = err.errors[errName].message;
		}
	}

	return message;
};

/**
 * Create a Hashtag
 */
exports.create = function(req, res) {
	var hashtag = new Hashtag(req.body);
	hashtag.user = req.user;

	hashtag.save(function(err) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(hashtag);
		}
	});
};

/**
 * Show the current Hashtag
 */
exports.read = function(req, res) {
	res.jsonp(req.hashtag);
};

/**
 * Update a Hashtag
 */
exports.update = function(req, res) {
	var hashtag = req.hashtag;

	hashtag = _.extend(hashtag, req.body);

	hashtag.save(function(err) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(hashtag);
		}
	});
};

/**
 * Delete an Hashtag
 */
exports.delete = function(req, res) {
	var hashtag = req.hashtag;

	hashtag.remove(function(err) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(hashtag);
		}
	});
};

/**
 * List of Hashtags
 */
exports.list = function(req, res) {
	Hashtag.find().sort('-created').populate('user', 'displayName').exec(function(err, hashtags) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(hashtags);
		}
	});
};

/**
 * Hashtag middleware
 */
exports.hashtagByID = function(req, res, next, id) {
	Hashtag.findById(id).populate('user', 'displayName').exec(function(err, hashtag) {
		if (err) return next(err);
		if (!hashtag) return next(new Error('Failed to load Hashtag ' + id));
		req.hashtag = hashtag;
		next();
	});
};

/**
 * Hashtag authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.hashtag.user.id !== req.user.id) {
		return res.send(403, 'User is not authorized');
	}
	next();
};