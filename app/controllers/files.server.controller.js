'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var _ = require('lodash');
var multiparty = require('multiparty');
var fs = require('fs');
var mv = require('mv');
var gm = require('gm');
var FFmpeg = require('fluent-ffmpeg');
FFmpeg.setFfmpegPath(process.env.HOME+'/bin/ffmpeg');

/**
 * Get the error message from error object
 */
var getErrorMessage = function(err) {
	var message = '';
	if (err.code) {
		switch (err.code) {
			case 11000:
			case 11001:
				message = 'File already exists';
				break;
			default:
				message = 'Something went wrong';
		}
	} else {
		for (var errName in err.errors) {
			if (err.errors[errName].message) message = err.errors[errName].message;
		}
	}
	return message;
};

/**
 * Upload a file
 */
exports.upload = function (req, res, next) {
  var form = new multiparty.Form();
  form.parse(req, function (err, fields, files) {
    var oldPath, filepath, newPath, type, file;
    if (files) {
      if (files.myFile && files.myFile[0]) {
        oldPath = files.myFile[0].path;
        type = files.myFile[0].headers['content-type'].split('/')[0];
        filepath = exports.getFilename(oldPath, type !== 'image');
        newPath = [__dirname, '..', '..', 'public', 'uploads', filepath].join('/');
        // Create File object
        file = {
          path: filepath,
          name: exports.getFilename(files.myFile[0].originalFilename, true),
          type: type
        };
      } else {
        err = { data: { message: 'Error uploading file. Please try again.' } };
      }
      if (err === null) {
        switch(type) {
        case 'image':
          gm(oldPath).autoOrient().write(newPath, function (err) {
            exports.completeFileSave(file, res, err);
          });
          break;
        case 'video':
          exports.convertVideo(oldPath, newPath, function (err) {
            if (err) {
              console.log(err);
            }
            exports.completeFileSave(file, res, err);
          });
          break;
        case 'audio':
          exports.convertAudio(oldPath, newPath, file, res);
          break;
        default:
          res.send({
            message: 'File is not a supported type - please upload images, video or audio only.'
          });
          break;
        }
      } else {
        res.send(err);
      }
    } else {
      res.send({ message: 'Server error. Please try again' });
    }
  });
};

/**
 * Get file name
 */
exports.getFilename = function (filepath, noExt) {
  var filename = filepath.split('/').pop();
  if (noExt) {
    filename = filename.split('.');
    if (filename.length > 1) {
      filename.pop();
    }
    filename = filename.join('.');
  }
  return filename;
};

/**
 * Convert a video to an HTML5-comptible format
 */
exports.convertVideo = function (from, to, callback) {
  console.log('Creating still frame');
  exports.createFrame(from, to, function (err) {
    if (!err) {
      /*console.log('Creating webm file');*/
      /*exports.createWebM(from, to, function (err) {*/
      /*if (!err) {*/
      /*console.log('webm converted');*/
      /*} else {*/
      /*console.log('Error converting to webm', err);*/
      /*}*/
      /*});*/
      console.log('Creating mp4 file');
      exports.createMp4(from, to, function (err) {
        if (!err) {
          console.log('mp4 converted');
        } else {
          console.log('Error converting to mp4', err);
        }
      });
    }
    callback(err);
  });
};

exports.createFrame = function (from, to, callback) {
  var command = new FFmpeg({ source: from });
  command
    .setStartTime(5)
    .takeFrames(1)
    .withSize('720x?')
    .toFormat('image2')
    .on('error', callback)
    .on('end', callback)
    .saveToFile(to+'.jpg');
};

exports.createWebM = function (from, to, callback) {
  var command = new FFmpeg({ source: from, });
  command
    .withVideoCodec('libvpx')
    .withVideoBitrate('768k')
    .withSize('720x?')
    .withAudioCodec('libvorbis')
    .withAudioBitrate('112k')
    .withAudioChannels(2)
    .withAudioFrequency(44100)
    .toFormat('webm')
    .on('error', callback)
    .on('end', callback)
    .saveToFile(to+'.webm');
};

exports.createMp4 = function (from, to, callback) {
  var command = new FFmpeg({ source: from, });
  command
    .withVideoCodec('libx264')
    .withSize('720x?')
    .withAudioCodec('aac')
    .withAudioBitrate('96k')
    .withAudioChannels(2)
    .toFormat('mp4')
    .addOptions([ '-preset slow', '-crf 22' ])
    .on('error', callback)
    .on('end', callback)
    .saveToFile(to+'.mp4');
};

/**
 * Convert an audio file to an HTML5-comptible format
 */
exports.convertAudio = function (from, to, res) {
};

exports.completeFileSave = function (file, res, err) {
  if (err) {
    console.log('Error', err, 'File', file);
    return res.send(400, {
      message: getErrorMessage(err)
    });
  } else {
    return res.send({
      file: {
        filepath: file.path,
        filetype: file.type
      }
    });
  }
};

/**
 * Delete an File
 */
exports.delete = function(req, res) {
	var file = req.file;
  var filepath = [__dirname, '..', '..', 'public', 'uploads', file.path].join('/');
  var paths = [];
  if (file.type === 'video') {
    paths.push(filepath+'.jpg');
    paths.push(filepath+'.mp4');
  } else {
    paths.push(filepath);
  }
  exports.deleteFile(paths, function (e) {
    if (e && e.errno !== 34) {
      console.log(e);
      return res.send(400, {
        message: getErrorMessage(e)
      });
    } else {
      res.jsonp(file);
    }
  });
};

exports.deleteFile = function (paths, callback, err) {
  var path = paths.shift();
  fs.unlink(path, function (e) {
    if (paths.length > 0) {
      exports.deleteFile(paths, callback, err || e);
    } else {
      callback(err || e);
    }
  });
};

/**
 * List of Files
 */
exports.list = function(req, res) {
};

