'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Hashtag Schema
 */
var HashtagSchema = new Schema({
	tag: {
		type: String,
		default: '',
		required: 'Please enter a hashtag',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
  description: {
    type: String,
    default: '',
    required: 'Please enter a description'
  },
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Hashtag', HashtagSchema);
