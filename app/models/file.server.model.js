'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * File Schema
 */
var FileSchema = new Schema({
	name: {
		type: String
	},
	created: {
		type: Date,
		default: Date.now
	},
  path: {
    type: String
  },
  type: {
    type: String,
    default: ''
  }
});

mongoose.model('File', FileSchema);
