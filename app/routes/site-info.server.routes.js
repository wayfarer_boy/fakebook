'use strict';

module.exports = function(app) {
	// Routing logic   
	// ...
	var siteInfo = require('../../app/controllers/site-info');
	app.route('/info').get(siteInfo.read);
};
