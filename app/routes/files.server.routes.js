'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var files = require('../../app/controllers/files');

  app.route('/files/upload')
    .post(users.requiresLogin, files.upload);
};
