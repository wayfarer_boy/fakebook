'use strict';

//Setting up route
angular.module('files').config(['$stateProvider',
	function($stateProvider) {
		// Files state routing
		$stateProvider.
		state('listFiles', {
			url: '/files',
			templateUrl: 'modules/files/views/list-files.client.view.html'
		});
	}
]);
