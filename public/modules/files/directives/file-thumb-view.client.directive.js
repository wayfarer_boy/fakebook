'use strict';

angular.module('files').directive('fileThumbView', ['$timeout', 
	function($timeout) {
		return {
      templateUrl: 'modules/files/views/file-thumb-view.html',
			restrict: 'E',
      scope: {
        file: '=',
        resolution: '@',
        lazyload: '='
      },
			link: function postLink(scope, element, attrs) {
        if (scope.lazyload) {
          var $ = window.$;
          $timeout(function () {
            $(element).find('img').lazyLoadXT();
          },0);
        }
			}
		};
	}
]);
