'use strict';

angular.module('files').directive('uploader', [
	function() {
		return {
      templateUrl: 'modules/files/views/uploader.html',
			restrict: 'E',
      scope: {
        resolution: '@',
        choose: '@',
        replace: '@',
        extensions: '@',
        file: '=',
        update: '&onUpdate'
      },
			link: function postLink(scope, element, attrs) {
        var button = element.find('button.uploader-click');
        var input = element.find('input.file-selector');
        button.click(function (e) {
          e.preventDefault();
          input.click();
        });
			},
      controller: ['$scope', '$upload', '$timeout', 'Files', function ($scope, $upload, $timeout, Files) {
        $scope.options = {
          resolution: $scope.resolution || '320x',
          choose: $scope.choose || 'Choose a file',
          replace: $scope.replace || 'Replace file',
          extensions: $scope.extensions.toLowerCase() || 'jpg,png,gif,jpeg'.toLowerCase()
        };
        $scope.options.extensions = $scope.options.extensions.split(',');
        $scope.ext_re = /(?:\.([^.]+))?$/;

        $scope.onFileSelect = function ($file) {
          if ($file[0]) {
            var ext = $scope.ext_re.exec($file[0].name)[1];
            var match = -1;
            if (ext) {
              match = $scope.options.extensions.indexOf(ext.toLowerCase());
            }
            if (match > -1) {
              $scope.selectedFile = $file[0];
              $scope.upload = $upload.upload({
                url: 'files/upload',
                file: $scope.selectedFile,
                fileFormDataName: 'myFile'
              }).then(function (response) {
                if (response.data && response.data.message) {
                  $scope.error = response.data.message;
                } else {
                  if (!$scope.file) $scope.file = {};
                  angular.extend($scope.file, response.data.file);
                  if ($scope.update) {
                    $timeout($scope.update, 500);
                  }
                }
                $scope.progress = false;
                $scope.selectedFile = null;
              }, null, function (evt) {
                $scope.progress = parseInt(100.0 * (evt.loaded / evt.total));
                if ($scope.progress === 100) {
                  $scope.progress = false;
                }
              });
            } else {
              $scope.error = 'Only the following extensions are allowed: '+$scope.options.extensions.join(', ');
            }
          }
        };

      }]
		};
	}
]);
