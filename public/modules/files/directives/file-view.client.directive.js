'use strict';

angular.module('files').directive('fileView', [
	function() {
		return {
      templateUrl: 'modules/files/views/file-view.html',
			restrict: 'E',
      scope: {
        file: '=',
        resolution: '@',
        fallback: '='
      }
		};
	}
]);
