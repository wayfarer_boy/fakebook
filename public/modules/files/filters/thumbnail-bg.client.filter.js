'use strict';

angular.module('files').filter('thumbnailBg', [
	function() {
		return function(input) {
			// Thumbnail bg directive logic 
			// ...

      if (input.filetype === 'image') {
        return input.filepath;
      }
      return input.filepath+'.jpg';
		};
	}
]);
