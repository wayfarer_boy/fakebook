'use strict';

angular.module('files').filter('videoSrcMp4', [
	function() {
		return function(input) {
			return '/uploads/'+input+'.mp4';
		};
	}
]);

angular.module('files').filter('videoSrcWebm', [
	function() {
		return function(input) {
			return '/uploads/'+input+'.webm';
		};
	}
]);
