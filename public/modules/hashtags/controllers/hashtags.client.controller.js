'use strict';

// Hashtags controller
angular.module('hashtags')
/*.factory('mySocket', function (socketFactory) {*/
/*return socketFactory();*/
/*})*/
  .controller('HashtagsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Hashtags', 'Mysocket',
    function($scope, $stateParams, $location, Authentication, Hashtags, Mysocket) {
      $scope.authentication = Authentication;
      $scope.selectedTag = $stateParams.hashtag;

      // Create new Hashtag
      $scope.create = function() {
        // Create new Hashtag object
        var hashtag = new Hashtags({
          tag: this.tag,
          description: this.description
        });
        // Redirect after save
        hashtag.$save(function(response) {
          Mysocket.emit('new:hashtag');
          $scope.hashtags.splice(0,0,hashtag);
          $location.path('hashtags');
        }, function(errorResponse) {
          $scope.error = errorResponse.data.message;
        });
        // Clear form fields
        this.tag = '';
        this.description = '';
      };

      // Remove existing Hashtag
      $scope.remove = function(hashtag) {
        if (hashtag) {
          Mysocket.emit('remove:hashtag', {id:hashtag._id});
          hashtag.$remove();
          for (var i in $scope.hashtags) {
            if ($scope.hashtags[i] === hashtag) {
              $scope.hashtags.splice(i, 1);
            }
          }
        } else {
          Mysocket.emit('remove:hashtag', {id:$scope.hashtag._id});
          $scope.hashtag.$remove(function() {
            $location.path('hashtags');
          });
        }
      };

      // Update existing Hashtag
      $scope.update = function() {
        var hashtag = $scope.hashtag;
        hashtag.$update(function() {
          Mysocket.emit('update:hashtag', {id:hashtag._id});
        }, function(errorResponse) {
          $scope.error = errorResponse.data.message;
        });
      };

      $scope.remoteUpdate = function (op) {
        return function (ev) {
          if ($scope.hashtags) {
            console.log('Remote updated', op, ev);
            switch (op) {
              case 'new':
                Hashtags.query(function (data) {
                  $scope.hashtags.splice(0, 0, data[0]);
                });
                break;
              default:
                angular.forEach($scope.hashtags, function (v,k) {
                  if (v._id === ev.id) {
                    if (op === 'remove') {
                      $scope.hashtags.splice(k,1);
                    } else {
                      $scope.hashtags[k] = Hashtags.get({
                        hashtagId: ev.id
                      });
                    }
                  }
                });
                break;
            }
          }
        };
      };

      $scope.deleteAll = function () {
        $scope.hashtags = Hashtags.query(function () {
          $scope.hashtags.forEach(function (tag) {
            tag.$remove();
          });
        });
      };

      // Find a list of Hashtags
      $scope.find = function() {
        Hashtags.query(function (data) {
          $scope.hashtags = data;
        });
        Mysocket.on('new:hashtag', $scope.remoteUpdate('new'));
        Mysocket.on('remove:hashtag', $scope.remoteUpdate('remove'));
        Mysocket.on('update:hashtag', $scope.remoteUpdate('update'));
      };

      // Find existing Hashtag
      $scope.findOne = function() {
        $scope.hashtag = Hashtags.get({
          hashtagId: $stateParams.hashtagId
        });
      };

    }
  ]);
