'use strict';

(function() {
	// Hashtags Controller Spec
	describe('Hashtags Controller Tests', function() {
		// Initialize global variables
		var HashtagsController,
			scope,
			$httpBackend,
			$stateParams,
			$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Hashtags controller.
			HashtagsController = $controller('HashtagsController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Hashtag object fetched from XHR', inject(function(Hashtags) {
			// Create sample Hashtag using the Hashtags service
			var sampleHashtag = new Hashtags({
				name: 'New Hashtag'
			});

			// Create a sample Hashtags array that includes the new Hashtag
			var sampleHashtags = [sampleHashtag];

			// Set GET response
			$httpBackend.expectGET('hashtags').respond(sampleHashtags);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.hashtags).toEqualData(sampleHashtags);
		}));

		it('$scope.findOne() should create an array with one Hashtag object fetched from XHR using a hashtagId URL parameter', inject(function(Hashtags) {
			// Define a sample Hashtag object
			var sampleHashtag = new Hashtags({
				name: 'New Hashtag'
			});

			// Set the URL parameter
			$stateParams.hashtagId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/hashtags\/([0-9a-fA-F]{24})$/).respond(sampleHashtag);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.hashtag).toEqualData(sampleHashtag);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Hashtags) {
			// Create a sample Hashtag object
			var sampleHashtagPostData = new Hashtags({
				name: 'New Hashtag'
			});

			// Create a sample Hashtag response
			var sampleHashtagResponse = new Hashtags({
				_id: '525cf20451979dea2c000001',
				name: 'New Hashtag'
			});

			// Fixture mock form input values
			scope.name = 'New Hashtag';

			// Set POST response
			$httpBackend.expectPOST('hashtags', sampleHashtagPostData).respond(sampleHashtagResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Hashtag was created
			expect($location.path()).toBe('/hashtags/' + sampleHashtagResponse._id);
		}));

		it('$scope.update() should update a valid Hashtag', inject(function(Hashtags) {
			// Define a sample Hashtag put data
			var sampleHashtagPutData = new Hashtags({
				_id: '525cf20451979dea2c000001',
				name: 'New Hashtag'
			});

			// Mock Hashtag in scope
			scope.hashtag = sampleHashtagPutData;

			// Set PUT response
			$httpBackend.expectPUT(/hashtags\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/hashtags/' + sampleHashtagPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid hashtagId and remove the Hashtag from the scope', inject(function(Hashtags) {
			// Create new Hashtag object
			var sampleHashtag = new Hashtags({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Hashtags array and include the Hashtag
			scope.hashtags = [sampleHashtag];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/hashtags\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleHashtag);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.hashtags.length).toBe(0);
		}));
	});
}());