'use strict';

//Setting up route
angular.module('hashtags').config(['$stateProvider',
	function($stateProvider) {
		// Hashtags state routing
		$stateProvider.
		state('listHashtags', {
			url: '/hashtags',
			templateUrl: 'modules/hashtags/views/list-hashtags.client.view.html'
		}).
		state('createHashtag', {
			url: '/hashtags/create',
			templateUrl: 'modules/hashtags/views/create-hashtag.client.view.html'
		}).
		state('viewHashtag', {
			url: '/hashtags/:hashtagId',
			templateUrl: 'modules/hashtags/views/view-hashtag.client.view.html'
		}).
		state('editHashtag', {
			url: '/hashtags/:hashtagId/edit',
			templateUrl: 'modules/hashtags/views/edit-hashtag.client.view.html'
		});
	}
]);