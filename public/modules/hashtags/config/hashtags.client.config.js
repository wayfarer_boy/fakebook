'use strict';

// Configuring the Articles module
angular.module('hashtags').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem(3, 'topbar', 'Hashtags', 'tags', 'hashtags');
	}
]);
