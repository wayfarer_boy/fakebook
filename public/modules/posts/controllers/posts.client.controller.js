'use strict';

// Posts controller
angular.module('posts')
/*.factory('mySocket', function (socketFactory) {*/
/*return socketFactory();*/
/*})*/
  .controller('PostsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Posts', 'Files', 'Mysocket',
    function($scope, $stateParams, $location, Authentication, Posts, Files, Mysocket) {
      $scope.authentication = Authentication;
      $scope.selectedTag = $stateParams.hashtag;

      // Create new Post
      $scope.create = function() {
        if (this.tweet && this.tweet.length > 140) {
          $scope.error = 'Text is over 140 characters.';
        } else {
          var post = new Posts({
            tweet: this.tweet,
            file: this.file
          });
          // Redirect after save
          post.$save(function(response) {
            Mysocket.emit('new:post');
            $location.path('posts');
          }, function(errorResponse) {
            $scope.error = errorResponse.data.message;
          });
            // Clear form fields
          this.title = '';
          this.file = null;
        }
      };

      // Remove existing Post
      $scope.remove = function(post) {
        if (post) {
          Mysocket.emit('remove:post', {id:post._id});
          post.$remove();
          for (var i in $scope.posts) {
            if ($scope.posts[i] === post) {
              $scope.posts.splice(i, 1);
            }
          }
        } else {
          $scope.post.$remove(function() {
            Mysocket.emit('remove:post', {id:$scope.post._id});
            $location.path('posts');
          });
        }
      };

      // Update existing Post
      $scope.update = function() {
        var post = $scope.post;
        post.$update(function() {
          Mysocket.emit('update:post', {id:post._id});
          $location.path('posts/' + post._id);
        }, function(errorResponse) {
          $scope.error = errorResponse.data.message;
        });
      };

      $scope.remoteUpdate = function (op) {
        return function (ev) {
          if ($scope.posts) {
            console.log('Remote updated', op, ev);
            switch (op) {
              case 'new':
                Posts.query(function (data) {
                  $scope.posts.splice(0, 0, data[0]);
                });
                break;
              default:
                angular.forEach($scope.posts, function (v,k) {
                  if (v._id === ev.id) {
                    if (op === 'remove') {
                      $scope.posts.splice(k,1);
                    } else {
                      $scope.posts[k] = Posts.get({
                        postId: ev.id
                      });
                    }
                  }
                });
                break;
            }
          }
        };
      };

      $scope.deleteAll = function () {
        $scope.posts = Posts.query(function () {
          $scope.posts.forEach(function (post) {
            post.$remove();
          });
        });
      };

      // Find a list of Posts
      $scope.find = function() {
        $scope.posts = Posts.query();
        Mysocket.on('new:post', $scope.remoteUpdate('new'));
        Mysocket.on('remove:post', $scope.remoteUpdate('remove'));
        Mysocket.on('update:post', $scope.remoteUpdate('update'));
      };

      // Find existing Post
      $scope.findOne = function() {
        $scope.post = Posts.get({
          postId: $stateParams.postId
        });
      };

      $scope.fileRemove = function () {
        var post = $scope.post;
        var file = new Files(post.file);
        file.$remove();
      };

      $scope.expandPost = function (index, expand) {
        $scope.posts[index].expanded = expand;
      };

      $scope.remainingLength = function (tweet) {
        if (tweet) {
          return 140 - tweet.length;
        }
        return 140;
      };

      $scope.checkChars = function (tweet) {
        var valid = true;
        if (tweet) {
          valid = tweet.length > 140;
          if (valid) {
            $scope.error = '';
          }
        }
        return valid;
      };

      $scope.addToField = function (tag) {
        if (!$scope.tweet) $scope.tweet = '';
        $scope.tweet = $scope.tweet + ' #' + tag + ' ';
      };
    }
  ]);
