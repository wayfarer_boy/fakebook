'use strict';

// Configuring the Articles module
angular.module('posts').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem(2, 'topbar', 'Posts', 'list', '');
		Menus.addMenuItem(1, 'topbar', 'Add', 'plus', 'posts/create');
	}
]);
