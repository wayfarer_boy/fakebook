'use strict';

angular.module('posts').filter('hashtag', ['$stateParams', 
	function($stateParams) {
		return function(posts) {
      var newPosts = [];
      if ($stateParams.hashtag) {
        angular.forEach(posts, function (post,k) {
          if (post.tweet) {
            var all_re = /\S*#[a-zA-Z0-9]+/gi;
            var arr;
            while((arr = all_re.exec(post.tweet)) !== null) {
              if (arr[0].substr(1) === $stateParams.hashtag) {
                newPosts.push(post);
              }
            }
          }
        });
        return newPosts;
      }
      return posts;
		};
	}
]);
