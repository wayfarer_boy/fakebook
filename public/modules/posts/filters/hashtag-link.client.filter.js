'use strict';

angular.module('posts').filter('hashtagLink', [ '$filter',
	function($filter) {
		return function(text) {
      if (!text) return text;

      var replacedText = $filter('linky')(text);
      // replace #hashtags and send them to twitter
      var replacePattern1 = /(^|\s)#(\w*[a-zA-Z_]+\w*)/gim;
      replacedText = text.replace(replacePattern1, '$1<a href="/#!/posts/tags/$2">#$2</a>');
      // replace @mentions but keep them to our site
      var replacePattern2 = /(^|\s)\@(\w*[a-zA-Z_]+\w*)/gim;
      replacedText = replacedText.replace(replacePattern2, '$1<a href="https://twitter.com/$2">@$2</a>');
      return replacedText;
		};
	}
]);
