'use strict';

var moment = window.moment || {};

moment.lang('en', {
  relativeTime : {
    future: 'in %s',
    past:   '%s',
    s:  'just now',
    m:  '1m',
    mm: '%dm',
    h:  '1h',
    hh: '%dh',
    d:  '1d',
    dd: '%dd',
    M:  '1m',
    MM: '%dm',
    y:  '1y',
    yy: '%dy'
  }
});
