'use strict';

angular.module('posts').directive('lazyLoadImg', [ '$timeout',
	function($timeout) {
		return {
			restrict: 'C',
			link: function postLink(scope, element) {
        var $ = window.$;
        $timeout(function () {
          $(element).lazyLoadXT();
        },0);
			}
		};
	}
]);
