'use strict';

angular.module('core').factory('Mysocket', [ 'socketFactory',
	function(socketFactory) {
    return socketFactory();
	}
]);
