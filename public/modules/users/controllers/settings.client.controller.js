'use strict';

angular.module('users').controller('SettingsController', ['$scope', '$http', '$location', '$stateParams', '$modal', 'Users', 'AdminUsers', 'Authentication',
	function($scope, $http, $location, $stateParams, $modal, Users, AdminUsers, Authentication) {
		$scope.user = Authentication.user;

		// If user is not signed in then redirect back home
		if (!$scope.user) $location.path('/');

		// Check if there are additional accounts 
		$scope.hasConnectedAdditionalSocialAccounts = function(provider) {
			for (var i in $scope.user.additionalProvidersData) {
				return true;
			}
			return false;
		};
		
		// Check if provider is already in use with current user
		$scope.isConnectedSocialAccount = function(provider) {
			return $scope.user.provider === provider || ($scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider]);
		};
		// Remove a user social account
		$scope.removeUserSocialAccount = function(provider) {
			$scope.success = $scope.error = null;
			$http.delete('/users/accounts', {
				params: {
					provider: provider
				}
			}).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.user = Authentication.user = response;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		// Update a user profile
		$scope.updateUserProfile = function() {
			$scope.success = $scope.error = null;
			var user = $scope.profile;
      var isAdmin = $stateParams.userId;
			user.$update(function(response) {
				$scope.success = true;
        if (isAdmin) {
          $location.path('admin/users');
        } else {
          Authentication.user = response;
        }
			}, function(response) {
				$scope.error = response.data.message;
			});
		};

		// Change user password
		$scope.changeUserPassword = function() {
			$scope.success = $scope.error = null;
      var url;
      if ($stateParams.userId) {
        url = '/admin/users/'+$stateParams.userId+'/password';
      } else {
        $scope.profile = null;
        url = '/users/password';
      }
			$http.post(url, $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
        $scope.findOne();
				$scope.success = true;
				$scope.passwordDetails = null;
			}).error(function(response) {
        $scope.findOne();
				$scope.error = response.message;
			});
		};

    $scope.signup = function() {
      $http.post('/auth/signup', $scope.profile).success(function(response) {
        // redirect to the index page
        $location.path('admin/users');
      }).error(function(response) {
        $scope.error = response.message;
      });
    };

    $scope.initRoles = function () {
      $scope.roles = {
        user: true,
        admin: $scope.profile.roles.indexOf('admin') > -1
      };
    };

    // Change user roles (admin only)
    $scope.changeRoles = function () {
			$scope.success = $scope.error = null;
      angular.forEach($scope.roles, function (v,k) {
        if (v && $scope.profile.roles.indexOf(k) === -1) {
          $scope.profile.roles.push(k);
          console.log('Added '+k+' role');
        } else if (!v && $scope.profile.roles.indexOf(k) > -1) {
          $scope.profile.roles.splice($scope.profile.roles.indexOf(k), 1);
          console.log('Removed '+k+' role');
        }
      });
      console.log($scope.profile.roles);
    };

    // Get single user
    $scope.findOne = function () {
      if ($stateParams.userId) {
        $scope.profile = AdminUsers.get({
          userId: $stateParams.userId
        }, function () {
          $scope.initRoles();
        });
      } else {
        $scope.profile = new Users($scope.user);
        $scope.initRoles();
      }
    };

    // List all users
    $scope.findAll = function () {
      $scope.users = AdminUsers.query();
    };

    $scope.reallyDelete = function (user) {
      var modal = $modal.open({
        templateUrl: 'modules/core/views/really-delete.client.view.html'
      });
      modal.result.then(function () {
        $scope.remove(user);
      });
    };

    // Remove existing user
    $scope.remove = function(user) {
      if (user) {
        console.log('Removing user object', user.username, user._id);
        AdminUsers.delete({
          userId: user._id
        }, function () {
          $location.path('admin/users');
        });
      } else {
        console.log('Removing scope.user', $scope.user.username);
        $scope.user.$remove(function() {
          $location.path('admin/users');
        });
      }
    };

    $scope.canEdit = function (user) {
      var isNotSame = user._id !== $scope.user._id;
      var isAl = $scope.user.email === 'generamics@gmail.com';
      var onlyUser = user.roles.indexOf('user') > -1 && user.roles.length === 1;
      return isNotSame && (isAl || onlyUser);
    };

    $scope.checkRoles = function () {
      return $scope.user.roles.indexOf('admin') > -1;
    };
}]);
