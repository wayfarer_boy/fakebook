'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
	function($stateProvider) {
		// Users state routing
		$stateProvider.
		state('profile', {
			url: '/settings/profile',
			templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
		}).
		state('password', {
			url: '/settings/password',
			templateUrl: 'modules/users/views/settings/change-password.client.view.html'
		}).
		state('accounts', {
			url: '/settings/accounts',
			templateUrl: 'modules/users/views/settings/social-accounts.client.view.html'
		}).
		state('signin', {
			url: '/login',
			templateUrl: 'modules/users/views/signin.client.view.html'
		}).
		state('signup', {
			url: '/register',
			templateUrl: 'modules/users/views/signup.client.view.html'
		}).
		state('listUsers', {
			url: '/admin/users',
			templateUrl: 'modules/users/views/list-users.client.view.html'
		}).
		state('addUser', {
			url: '/admin/users/add',
			templateUrl: 'modules/users/views/signup.client.view.html'
		}).
		state('editUser', {
      url: '/admin/users/:userId/edit',
			templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
		}).
		state('editPassword', {
      url: '/admin/users/:userId/password',
			templateUrl: 'modules/users/views/settings/change-password.client.view.html'
		});
	}
]);
