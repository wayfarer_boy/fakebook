'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', ['$resource', function($resource) {
  return $resource('users/:userId', {}, {
    update: {
      method: 'PUT'
    }
  });
}]);

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('AdminUsers', ['$resource', function($resource) {
  return $resource('admin/users/:userId', {
    userId: '@_id'
  }, {
    update: {
      method: 'PUT'
    }
  });
}]);
