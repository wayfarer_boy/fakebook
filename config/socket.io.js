'use strict';

/**
 * Module dependencies
 */

var http = require('http');
var socketio = require('socket.io');
var _ = require('lodash');

module.exports = function (app) {
  var server = http.createServer(app);
  var io = socketio.listen(server);

  var socketListen = function (socket, action, type) {
    socket.on(action+':'+type, function (data) {
      socket.broadcast.emit(action+':'+type, data);
    });
  };

  io.on('connection', function (socket) {
    var actions = ['new', 'remove', 'update'];
    var types = ['post', 'hashtag'];
    for (var type in types) {
      for (var action in actions) {
        socketListen(socket, actions[action], types[type]);
      }
    }
  });

  return server;
};

