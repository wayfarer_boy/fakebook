'use strict';

module.exports = {
	app: {
		title: 'fakebook',
		description: 'Full-Stack JavaScript with MongoDB, Express, AngularJS, and Node.js',
		keywords: 'MongoDB, Express, AngularJS, Node.js'
	},
	port: process.env.PORT || 3000,
	templateEngine: 'swig',
	sessionSecret: 'MEAN',
	sessionCollection: 'sessions',
	assets: {
		lib: {
			css: [
				'public/lib/bootstrap/dist/css/bootstrap.css',
				'public/lib/bootstrap/dist/css/bootstrap-theme.css',
			],
			js: [
        'public/lib/socket.io-client/socket.io.js',
        'public/lib/jquery/dist/jquery.js',
        'public/modules/posts/js/lazyload-options.js',
        'public/lib/lazyloadxt/dist/jquery.lazyloadxt.js',
        'public/lib/lazyloadxt/dist/jquery.lazyloadxt.srcset.js',
        'public/modules/files/js/angular-file-upload-paths.js',
        'public/lib/ng-file-upload/angular-file-upload-shim.js',
				'public/lib/angular/angular.js',
        'public/lib/ng-file-upload/angular-file-upload.js',
				'public/lib/angular-resource/angular-resource.js', 
				'public/lib/angular-cookies/angular-cookies.js',  
				'public/lib/angular-animate/angular-animate.js', 
				'public/lib/angular-touch/angular-touch.js', 
				'public/lib/angular-sanitize/angular-sanitize.js', 
        'public/lib/angular-socket-io/socket.js',
				'public/lib/angular-ui-router/release/angular-ui-router.js',
				'public/lib/angular-ui-utils/ui-utils.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
        'public/lib/moment/moment.js',
        'public/modules/posts/js/moment-humanise.js',
        'public/lib/angular-moment/angular-moment.js'
			]
		},
		css: [
      'public/lib/fontawesome/css/font-awesome.css',
			'public/modules/**/css/*.css'
		],
		js: [
			'public/config.js',
			'public/application.js',
			'public/modules/*/*.js',
			'public/modules/*/*[!tests]*/*.js'
		],
		tests: [
			'public/lib/angular-mocks/angular-mocks.js',
			'public/modules/*/tests/*.js'
		]
	}
};
